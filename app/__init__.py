from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager
from flask.ext.openid import OpenID
from flask.ext.babel import Babel, lazy_gettext
from config import BASE_DIR
import os

app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)
lm = LoginManager()
lm.init_app(app)
lm.login_view = 'login'
lm.login_message = lazy_gettext('Please log in to access this page.')
oid = OpenID(app, os.path.join(BASE_DIR, 'tmp'))
babel = Babel(app)


from app.users.views import mod as usersModule
from app.products.views import mod as productsModule
from app import views

app.register_blueprint(usersModule)
app.register_blueprint(productsModule)