from flask import render_template, g, session, url_for, redirect
from flask.ext.login import login_user, logout_user, current_user, \
	login_required
from app import app, db, lm
from app.users.models import User

@app.before_request
def before_request():
	g.user = current_user

@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
	return redirect(url_for('products.home'))

@app.errorhandler(404)
def notfound_error(error):
	return render_template('404.html'), 404

@app.errorhandler(500)
def internal_error(error):
	db.session.rollback()
	return render_template('500.html'), 500
