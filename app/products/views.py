from flask import Blueprint, request, render_template, flash, g, session, \
	redirect, url_for, send_from_directory
from flask.ext.login import login_user, logout_user, current_user, \
	login_required
from flask.ext.babel import gettext
from werkzeug import check_password_hash, generate_password_hash
from app import app, db, lm
from app.products.models import Product, category
from app.users.models import User
from app.products.forms import UploadForm, EditForm, DeleteForm
import math
import os

from werkzeug.utils import secure_filename

# TODO: Use flash() to display error messages

mod = Blueprint('products', __name__, url_prefix='/products')

def allowed_file(filename):
	return '.' in filename and \
		filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']

@lm.user_loader
def load_user(id):
	return User.query.get(int(id))

@mod.before_request
def before_request():
	g.user = current_user

@mod.route('/')
@mod.route('/get/page/<int:page>')
def home(page=1):
	product_count = Product.query.count()
	products = Product.query.paginate(page, per_page=12).items
	return render_template(
		'products/all.html',
		products=products,
		pages=int(math.ceil(product_count / float(12))),
		current_page=page
	)
	

@mod.route('/id/<int:id>')
def get_by_id(id):
	product = Product.query.filter_by(id=id).first()
	if product == None:
		flash(gettext('Product not found!'))
		return redirect(url_for('index'))
	return render_template(
		'products/product.html',
		product=product
	)

@mod.route('/search/<query>')
def search(query):
	pass

@mod.route('/upload', methods=['GET', 'POST'])
@login_required
def upload_product():
	user_id = User.query.filter_by(name=g.user.name).first().id
	form = UploadForm()
	if form.validate_on_submit():
		imagefile = request.files[form.imagefile.name]
		filename = None
		if imagefile and allowed_file(imagefile.filename):
			filename = secure_filename(imagefile.filename)
			imagefile.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
		product = Product(
			form.name.data, form.price.data, user_id,
			description=form.description.data,
			quantity=form.quantity.data,
			imagefilename=filename)
		db.session.add(product)
		db.session.commit()
		flash(gettext('Product successfully added!'))
		return redirect(url_for('products.home'))
	return render_template(
		'products/upload.html',
		form=form
	)

@mod.route('/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_product(id):
	form = EditForm()
	if form.validate_on_submit():
		pass
	return render_template(
		'products/edit.html',
		form=form
	)

@mod.route('/delete/id/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_product(id):
	form = DeleteForm()
	if form.validate_on_submit():
		pass
	return render_template(
		'products/delete.html',
		form=form
	)

# Serving images
@mod.route('/image/<filename>')
def give_image(filename):
	return send_from_directory(app.config['UPLOAD_FOLDER'],
			filename)