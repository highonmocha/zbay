from flask.ext.wtf import Form
from wtforms import TextField, TextAreaField, IntegerField, RadioField, \
	FileField
from wtforms.validators import Required, Length
from flask.ext.babel import gettext
from app.products.models import Product

# TODO: More fields, confirm NumberField / RadioField, 
#	default values for field, form inheritance, select / option fields,
#	passing values to Forms,

# TODO: use __init__ to get values, validate method for validation

class UploadForm(Form):
	name = TextField('Name', validators=[Required(), Length(min=6, max=30)])
	price = IntegerField('Price', validators=[Required()])
	description = TextAreaField('Description')
	quantity = IntegerField('Quantity')
	imagefile = FileField('Image')

	def __init__(self, *args, **kwargs):
		Form.__init__(self, *args, **kwargs)

	def validate(self):
		# aman commented this, because 
		# he found /upload wasnt working because of this.
		if not Form.validate(self):
			return False
		if self.price.data < 0:
			self.price.errors.append(
				gettext('Price should be greater than 0'))
			return False
		if self.quantity.data is None or self.quantity.data == '':
			self.quantity.data = 1
		if self.quantity.data < 0:
			self.quantity.errors.append(
				gettext('Quantity cannot be less than 1'))
			return false
		return True

class EditForm(UploadForm):
	category = TextField()

class DeleteForm(Form):
	confirm = RadioField('Confirm')
