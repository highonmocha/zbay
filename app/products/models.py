from app import db
from app.users.models import User
from datetime import datetime

category = db.Table('category', 
	db.Column('id', db.Integer, primary_key=True), 
	db.Column('name', db.String(24))
)

# TODO: ADD IMAGE FIELD
class Product(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(30))
	description = db.Column(db.Text)
	price = db.Column(db.Integer)
	owner = db.relationship('User', backref='user') 
	owner_id = db.Column(db.Integer, db.ForeignKey('user.id'))
	publish_date = db.Column(db.DateTime, default=datetime.utcnow())
	quantity = db.Column(db.Integer, default=1)
	views = db.Column(db.Integer, default=1)
	imagefilename = db.Column(db.String(30))

	# TODO: Have 'General' category that is default
	category_id = db.Column(db.Integer, db.ForeignKey('category.id'))

	def __init__(self, name, price, owner_id, description=None, quantity=1, imagefilename=None):
		self.name = name
		self.price = price
		self.owner_id = owner_id
		self.description = description
		self.quantity = quantity
		self.imagefilename = imagefilename

	def __repr__(self):
		return '<Product %r>' % (self.name)
