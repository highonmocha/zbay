from flask import Blueprint, request, render_template, flash, g, session, \
	redirect, url_for
from flask.ext.login import login_user, logout_user, current_user, \
	login_required
from flask.ext.babel import gettext
from werkzeug import check_password_hash, generate_password_hash
from app import app, db, lm, oid, babel
from app.users.models import User
from app.users.forms import LoginForm, EditForm
import app.users.constants as USER

from string import letters
from random import sample

NICK_MAX_LENGTH = 24
mod = Blueprint('users', __name__, url_prefix='/users')

@lm.user_loader
def load_user(id):
	return User.query.get(int(id))

@mod.before_request
def before_request():
	g.user = current_user

@mod.route('/me')
@login_required
def home():
	return render_template(
		'users/profile.html', 
		user=g.user
	)

@mod.route('/login', methods=['GET', 'POST'])
@oid.loginhandler
def login():
	if g.user is not None and g.user.is_authenticated():
		return redirect(url_for('index'))
	form = LoginForm()
	if form.validate_on_submit():
		return oid.try_login(form.openid.data, ask_for=['nickname', 'email'])
	return render_template(
		'users/login.html', 
		title='Sign In', 
		form=form, 
		providers=app.config['OPENID_PROVIDERS']
	)

@oid.after_login
def after_login(resp):
	if resp.email is None or resp.email == '':
		return redirect(url_for('users.login'))
	user = User.query.filter_by(email=resp.email).first()
	if user is None:
		nick = resp.nickname
		if nick is None or nick == '':
			nick = resp.email.split('@')[0][:NICK_MAX_LENGTH]
		n = User.query.filter_by(name=nick).first()
		
		# If the same nickname already exists, append random letters
		if n is not None:
			mod_nick = ('_' + ''.join(sample(letters, 10))) + nick
			nick = mod_nick[:NICK_MAX_LENGTH]
		user = User(name=nick, email=resp.email)
		db.session.add(user)
		db.session.commit()
	login_user(user)
	return redirect(request.args.get('next') or url_for('index'))

@mod.route('/logout')
def logout():
	logout_user()
	return redirect(url_for('index'))

@mod.route('/get/<name>')
def get_by_name(name):
	user = User.query.filter_by(name=name).first()
	if user and user.name == g.user.name:
		flash(gettext('Trying to view your own profile. So smart.'))
		return redirect(url_for('users.home'))
	if user == None or user.visibility == USER.PRIVATE:
		flash(
			gettext('The profile you requested is private or doesn\'t exist.'))
		return redirect(url_for('users.home'))
	return render_template(
		'users/show_profile.html',
		user=user
	)

@mod.route('/edit', methods=['GET', 'POST'])
@login_required
def edit():
	form = EditForm(g.user.name)
	if form.validate_on_submit():
		g.user.name = form.name.data
		g.user.about = form.about.data
		g.user.visibility = 0 if form.visibility.data == True else 1
		db.session.add(g.user)
		db.session.commit()
		return redirect(url_for('users.home'))
	elif request.method != 'POST':
		form.name.data = g.user.name
		form.about.data = g.user.about
		form.visibility.data = True if g.user.visibility == 0 else False
	return render_template(
		'users/edit.html',
		form=form
	)

