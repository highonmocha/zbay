from app import db
from app.users import constants as USER
from datetime import datetime

# TODO: Include password field for manual (email) registration

class User(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(24), unique=True)
	
	# TODO: Check RFC for max length of an email
	email = db.Column(db.String(120), index=True, unique=True)
	about = db.Column(db.String(200))
	visibility = db.Column(db.Integer, default=USER.PRIVATE)
	rating_points = db.Column(db.Integer, default=0)
	rating_times = db.Column(db.Integer, default=0)
	join_date = db.Column(db.DateTime, default=datetime.utcnow())

	def __init__(self, name = None, email = None):
		self.name = name
		self.email = email

	def is_authenticated(self):
		return True

	def is_active(self):
		return True

	def is_anonymous(self):
		return False

	def get_id(self):
		return unicode(self.id)

	def __repr__(self):
		return '<User %r>' % (self.name)
