from flask.ext.wtf import Form
from wtforms import TextField, PasswordField, TextAreaField, BooleanField
from wtforms.validators import Required, Length, Email, EqualTo
from flask.ext.babel import gettext
from app.users.models import User

# TODO: Use __init__ / validate methods for RegisterForm

class LoginForm(Form):
	openid = TextField('openid', validators=[Required()])

# Implement this after completing products module
class RegisterForm(Form):
	name = TextField('Name', validators=[Required()])
	email = TextField('Email', validators=[Required(), Email()])
	password = PasswordField('Password', validators=[Required()])
	confirm_password = PasswordField(
		'Repeat Password',
		validators=[
			Required(),
			EqualTo('password', message='Passwords must match')
		]
	)
	# optional: recaptcha = RecaptchaField()

# TODO: Check if entered username already exists

class EditForm(Form):
	name = TextField('Name', validators=[Length(min=4, max=24)])
	about = TextAreaField('About', validators=[Length(min=0, max=200)])
	visibility = BooleanField('Private Profile')

	def __init__(self, original_name, *args, **kwargs):
		Form.__init__(self, *args, **kwargs)
		self.original_name = original_name

	def validate(self):
		if not Form.validate(self):
			return False
		user = User.query.filter_by(name=self.name.data).first()
		if user is not None and user.name != self.original_name:
			self.name.errors.append(
				gettext('This name is alreay taken, please choose another one'))
			return False
		return True
