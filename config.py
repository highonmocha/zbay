import os

DEBUG = True
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

ADMINS = frozenset(['id@domain.tld'])

SECRET_KEY = 'you-will-never-guess'

OPENID_PROVIDERS = [{'name': 'Google',
  'url': 'https://www.google.com/accounts/o8/id'}]

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASE_DIR, 'app.db')
DATABASE_CONNECTION_OPTIONS = {}

THREADS_PER_PAGE = 2

CSRF_ENABLED = True
CSRF_SESSION_KEY = 'somethingimpossibletoguess'

RECAPTCHA_USE_SSL = False
RECAPTCHA_PUBLIC_KEY = ''
RECAPTCHA_PRIVATE_KEY = ''
RECAPTCHA_OPTIONS = {'theme': 'white'}

# File uploads

UPLOAD_FOLDER = os.path.join(BASE_DIR, 'media/uploads')
ALLOWED_EXTENSIONS = set(['jpg', 'png', 'gif', 'jpeg'])