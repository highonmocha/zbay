## Setup virtualenv and install required packages

_Consider zbay to be under __C:\\__, it can be anywhere._

```
C:\>virtualenv zbay
```

```
C:\zbay>Scripts\activate
```

```
(zbay) C:\zbay>pip install -r requirements.txt
```

## Create the database

```
(zbay) C:\zbay>python
```
```python
>>> from app import db
>>> db.create_all()
>>> db.session.commit()
```

## Run the app

```
(zbay) C:\zbay>python run.py
```